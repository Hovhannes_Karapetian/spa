import { goBack } from '../../router';
import './style.scss';

export const BackButton = () => {
  const backButton = document.createElement('div');
  backButton.className = 'back-btn';
  backButton.innerHTML = '<div><span>&larr;</span> Back</div>';
  backButton.onclick = () => {
    goBack();
  };
  return backButton;
};
