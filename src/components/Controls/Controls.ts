import { Next } from './Next';
import { Prev } from './Prev';
import { createDomElement } from '../../helper';
import './style.scss';

interface IControls {
  prevClick: () => void;
  nextClick: () => void;
  nextDisabled: boolean;
  prevDisabled: boolean;
}

export const Controls = ({ prevClick, nextClick, nextDisabled, prevDisabled }: IControls) => {
  const prev = Prev({ handleClick: prevClick, disable: prevDisabled });
  const next = Next({ handleClick: nextClick, disable: nextDisabled });
  const controls = createDomElement({ tag: 'div', attributes: { ['class']: 'control' }, children: [prev, next] });
  return controls;
};
