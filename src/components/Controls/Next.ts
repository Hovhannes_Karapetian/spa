interface INext {
  handleClick: () => void;
  disable?: boolean;
}

export const Next = ({ handleClick, disable = false }: INext) => {
  const next = document.createElement('div');
  next.innerHTML = ` <div class="next ${disable ? 'disable' : ''}">Next <span>&rarr;</span> </div>`;
  next.onclick = () => {
    if (!disable) {
      handleClick();
    }
  };
  return next;
};
