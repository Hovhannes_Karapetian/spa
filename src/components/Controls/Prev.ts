interface IPrev {
  handleClick: () => void;
  disable?: boolean;
}

export const Prev = ({ handleClick, disable = false }: IPrev) => {
  const prev = document.createElement('div');
  prev.innerHTML = ` <div class="prev ${disable ? 'disable' : ''}"><span>&larr;</span> Prev</div>`;
  prev.onclick = () => {
    if (!disable) {
      handleClick();
    }
  };
  return prev;
};
