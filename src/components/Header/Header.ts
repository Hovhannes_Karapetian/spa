import { routes } from '../../constant';
import { Links } from '../Link/Link';
import { createDomElement, toggleActiveLink } from '../../helper';
import './style.scss';

const headerRoute = [
  { route: routes.users, name: 'Users', ['class']: 'active' },
  { route: routes.repos, name: 'Repos' },
];

export const Header = () => {
  // toggle header active link
  window.addEventListener('popstate', toggleActiveLink);

  // create header links
  const headerLinks = headerRoute.map((item) => Links(item));
  const header = createDomElement({ tag: 'header', children: headerLinks });

  return header;
};
