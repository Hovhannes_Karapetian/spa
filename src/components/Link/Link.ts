import { navigateTo } from '../../router';

interface ILinks {
  route: string;
  name: string;
  className?: string;
}

export const Links = ({ route, name, className = '' }: ILinks) => {
  const link = document.createElement('a');
  link.className = className;
  link.innerText = name;
  link.href = route;
  link.addEventListener('click', (e) => {
    e.preventDefault();
    navigateTo(route);
  });
  return link;
};
