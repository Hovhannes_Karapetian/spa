import { createDomElement } from '../../../helper';
import './style.scss';

interface IReposItem {
  title: string;
  url: string;
}

export const ReposItem = ({ title, url }: IReposItem) => {
  const reposItemTitle = createDomElement({ tag: 'div', children: [title], attributes: { ['class']: 'title' } });

  const reposItem = createDomElement({
    tag: 'div',
    children: [reposItemTitle],
    attributes: { ['class']: 'repos-item' },
  });
  reposItem.onclick = () => {
    window.open(url, '_blank');
  };

  return reposItem;
};
