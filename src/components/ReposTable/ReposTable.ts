import { Controls } from '../../components';
import { ReposItem } from './ReposItem/ReposItem';
import { IReposItem } from './../../types';
import './style.scss';

interface IReposTable {
  page: number;
  data: IReposItem[];
  count: number;
  prevClick: () => void;
  nextClick: () => void;
  perPageCount: number;
}

export const ReposTable = ({ count, data, nextClick, prevClick, page, perPageCount }: IReposTable) => {
  const reposTable = document.createElement('div');
  reposTable.className = 'repos-content';

  const prevDisabled = page === 1 || count < perPageCount;
  const nextDisabled = count < perPageCount || page * perPageCount > count;
  const controls = Controls({ nextDisabled, nextClick, prevClick, prevDisabled });
  reposTable.prepend(controls);
  data.forEach((item) => {
    reposTable.appendChild(ReposItem({ title: item.name, url: item.html_url }));
  });

  return reposTable;
};
