import { createDomElement } from '../../helper';
import './style.scss';
interface ILinks {
  submit: (value: string) => void;
  placeholder: string;
  value?: string;
}

export const Search = ({ submit, placeholder, value = '' }: ILinks) => {
  // create input
  const input = createDomElement({
    tag: 'input',
    attributes: {
      placeholder,
      value,
      id: 'search',
    },
  }) as HTMLInputElement;

  // create button
  const btn = createDomElement({
    tag: 'button',
    attributes: {
      type: 'submit',
      value,
      ['class']: 'search-btn',
    },
    children: ['Search'],
  });

  const form = document.createElement('form');
  form.className = 'search-form';
  const inputNode = form.appendChild(input);
  form.appendChild(btn);

  form.addEventListener('submit', (e) => {
    e.preventDefault();
    submit(inputNode.value);
  });
  return form;
};
