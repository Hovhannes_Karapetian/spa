import { IUser } from '../../types';
import './style.scss';

export const UserInfo = (user: IUser) => {
  const userInfo = document.createElement('div');
  userInfo.className = 'user-info-container';
  const userInfoContent = `
    <div class="user-info"> 
        <img src=${user.avatar_url} />  
        <div class="title">${user.login}</div>
     </div>
    <div class="user-about">
        <div class="info-item">
        <div class="info-item-title">Public Repos:</div>
        <div class="info-item-desc">${user.public_repos}</div>
        </div>
        <div class="info-item">
            <div class="info-item-title">Public Gists:</div>
            <div class="info-item-desc">${user.public_gists}</div>
        </div>
        <div class="info-item">
            <div class="info-item-title">Followers:</div>
            <div class="info-item-desc">${user.followers}</div>
        </div>
    </div>`;
  userInfo.innerHTML = userInfoContent;

  return userInfo;
};
