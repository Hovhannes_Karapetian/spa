import { routes } from '../../constant';
import { navigateTo } from '../../router';
import { IUserItem } from '../../types';
import { createDomElement } from '../../helper';
import './style.scss';

export const UserItem = ({ login, avatar_url }: IUserItem) => {
  const userItemTitle = createDomElement({ tag: 'div', attributes: { ['class']: 'title' }, children: [login] });
  const userAvatar = createDomElement({ tag: 'img', attributes: { src: avatar_url } });

  const userItem = createDomElement({
    tag: 'div',
    attributes: { ['class']: 'user-item' },
    children: [userAvatar, userItemTitle],
  });

  userItem.onclick = () => {
    navigateTo(`${routes.user}?id=${login}`);
  };

  return userItem;
};
