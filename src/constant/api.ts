export const BASE_URL = 'https://api.github.com';
export const GET_REPOS_URL = `${BASE_URL}/search/repositories`;
export const GET_USERS_URL = `${BASE_URL}/search/users`;
export const GET_USER_URL = `${BASE_URL}/users`;
