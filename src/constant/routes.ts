export const routes = {
  main: '/',
  user: '/user',
  users: '/users',
  repos: '/repos',
  notFound: '/404',
} as const;
