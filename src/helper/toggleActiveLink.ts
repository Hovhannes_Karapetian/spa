import { routes } from '../constant';

export const toggleActiveLink = () => {
  const pathName = location.pathname;
  const elemUsers = document.querySelector(`a[href='${routes.users}']`);
  const elemRepos = document.querySelector(`a[href='${routes.repos}']`);
  if (elemUsers && elemRepos) {
    if (pathName === routes.users || pathName === routes.user) {
      elemUsers.className = 'active';
      elemRepos.className = '';
    } else if (pathName === routes.repos) {
      elemUsers.className = '';
      elemRepos.className = 'active';
    }
  }
};
