import { Header } from './components';
import { router } from './router';
import { toggleActiveLink } from './helper';
import './style/main.scss';

document.addEventListener('DOMContentLoaded', () => {
  const body = document.body;
  const header = Header();
  body.prepend(header);
  window.addEventListener('popstate', router);
  router();
  toggleActiveLink();
});
