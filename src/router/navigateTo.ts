export const navigateTo = (url: string) => {
  history.pushState(null, null as any, url);
  window.dispatchEvent(new Event('popstate'));
};

export const goBack = () => {
  history.back();
  window.dispatchEvent(new Event('popstate'));
};

export const writeParamsInHistory = (params: string) => {
  history.pushState({}, null as any, params);
};
