import { Repos, Users, User, NotFound } from '../views';
import { routes } from '../constant';

export const routerConfig = [
  { path: routes.main, view: Users },
  { path: routes.users, view: Users },
  { path: routes.repos, view: Repos },
  { path: routes.user, view: User },
];

export const router = () => {
  const findComponentByPath = routerConfig.find((item) => location.pathname === item.path);
  const app = document.querySelector('#app');

  if (findComponentByPath) {
    const Component = findComponentByPath.view;
    if (app) {
      app.innerHTML = '';
      app.append(Component());
    }
  } else {
    if (app) {
      app.innerHTML = '';
      app.append(NotFound());
    }
  }
};
