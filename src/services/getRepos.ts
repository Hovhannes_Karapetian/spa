import { IReposResponse } from '../types';
import { reposPerPages, GET_REPOS_URL } from '../constant';

interface IGetReposParams {
  value: string;
  page: number;
}
export const getRepos = ({ value, page }: IGetReposParams): Promise<IReposResponse> => {
  const url = `${GET_REPOS_URL}?q=${value}&per_page=${reposPerPages}&page=${page}`;

  return fetch(url)
    .then((res) => res.json())
    .catch((res) => res);
};
