import { IReposItem, IUser } from '../types';
import { userReposPerPages, GET_USERS_URL, GET_USER_URL } from '../constant';

interface IGetUserParams {
  userId: string;
}

interface IGetUserReposParams {
  page: number;
  userId: string;
}
export const getUserResponse = ({ userId }: IGetUserParams): Promise<IUser> => {
  const url = `${GET_USER_URL}/${userId}`;
  return fetch(url)
    .then((res) => res.json())
    .catch((res) => res);
};

export const getUserRepos = ({ page, userId }: IGetUserReposParams): Promise<IReposItem[]> => {
  const url = `${GET_USER_URL}/${userId}/repos?per_page=${userReposPerPages}&page=${page}`;
  return fetch(url)
    .then((res) => res.json())
    .catch((res) => res);
};
