import { IUserItemResponse } from '../types';
import { usersPerPages, GET_USERS_URL } from '../constant';

interface IGetUsersParams {
  value: string;
  page: number;
}
export const getUsers = ({ value, page }: IGetUsersParams): Promise<IUserItemResponse> => {
  const url = `${GET_USERS_URL}?q=${value}&per_page=${usersPerPages}&page=${page}`;
  return fetch(url)
    .then((res) => res.json())
    .catch((res) => res);
};
