export interface IReposItem {
  html_url: string;
  name: string;
}

export interface IReposResponse {
  incomplete_results: boolean;
  items: IReposItem[];
  total_count: number;
}
