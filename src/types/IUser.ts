export interface IUserItem {
  avatar_url: string;
  login: string;
}

export interface IUserItemResponse {
  incomplete_results: boolean;
  items: IUserItem[];
  total_count: number;
}

export interface IUser {
  avatar_url: string;
  created_at: string;
  public_gists: number;
  public_repos: number;
  followers: number;
  login: string;
  name: string;
}
