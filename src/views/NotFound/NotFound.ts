import { routes } from '../../constant';
import { Links } from '../../components';
import { createDomElement } from '../../helper';
import './style.scss';

export const NotFound = () => {
  const link = Links({ name: 'Go to Main', route: routes.main });
  const notFound = createDomElement({
    tag: 'div',
    children: ['Not Found', link],
    attributes: { ['class']: 'not-found' },
  });
  return notFound;
};
