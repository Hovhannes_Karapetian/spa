import { Search, ReposTable } from '../../components';
import { IReposItem } from '../../types';
import { getRepos } from '../../services';
import { reposPerPages, routes } from '../../constant';
import { writeParamsInHistory } from '../../router';

export const Repos = () => {
  // get initial state by history
  const locationParams = new URL(location.href).searchParams;
  let page = locationParams.get('page') ? Number(locationParams.get('page')) : 1;
  let value = locationParams.get('q') || '';

  const repos = document.createElement('div');

  const form = Search({ submit, placeholder: 'Search Repos' });
  repos.appendChild(form);
  const reposContent = document.createElement('div');
  const reposContentNode = repos.appendChild(reposContent);

  // go to next page
  const nextClick = () => {
    page++;
    getData();
  };

  // go to prev page
  const prevClick = () => {
    page--;
    getData();
  };

  //  handle form submit
  function submit(q: string) {
    if (value !== q) {
      value = q;
      page = 1;
      getData();
    }
  }

  // mount repos data in table
  const mountRepos = (data: IReposItem[], count: number) => {
    reposContentNode.innerHTML = '';
    const table = ReposTable({ nextClick, prevClick, page, count, data, perPageCount: reposPerPages });
    reposContentNode.prepend(table);
  };

  // get all repos
  const getData = () => {
    if (value) {
      // save state in history
      const newHistoryParams = `${routes.users}?page=${page}&q=${value}`;
      writeParamsInHistory(newHistoryParams);

      getRepos({ value, page })
        .then((res) => {
          mountRepos(res.items, res.total_count);
        })
        .catch(() => {});
    }
  };

  return repos;
};
