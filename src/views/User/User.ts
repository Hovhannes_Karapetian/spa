import { ReposTable, UserInfo, BackButton } from '../../components';
import { navigateTo } from '../../router';
import { routes, userReposPerPages } from '../../constant';
import { IReposItem, IUser } from '../../types';
import { getUserResponse, getUserRepos } from '../../services';

export const User = () => {
  let page = 1;
  let userReposTotalCount = 0;
  const userLogin = new URL(location.href);
  const userId = userLogin.searchParams.get('id') as string;

  const user = document.createElement('div');
  const userRepos = document.createElement('div');
  const userReposNode = user.appendChild(userRepos);

  const nextClick = () => {
    page++;
    getRepos();
  };
  const prevClick = () => {
    page--;
    getRepos();
  };

  const getUser = () => {
    getUserResponse({ userId })
      .then((res: IUser) => {
        userReposTotalCount = res.public_repos;
        const userInfo = UserInfo(res);
        user.prepend(userInfo);
        user.prepend(BackButton());
      })
      .catch(() => {
        navigateTo(routes.notFound);
      });
  };

  const getRepos = () => {
    getUserRepos({ page, userId })
      .then((res: IReposItem[]) => {
        userReposNode.innerHTML = '';
        const table = ReposTable({
          data: res,
          count: userReposTotalCount,
          perPageCount: userReposPerPages,
          nextClick,
          prevClick,
          page,
        });
        userReposNode.appendChild(table);
      })
      .catch(() => {
        navigateTo(routes.notFound);
      });
  };

  const init = () => {
    getUser();
    getRepos();
  };
  init();

  return user;
};
