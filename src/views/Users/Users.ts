import { routes, usersPerPages } from '../../constant';
import { Search, UserItem, Controls } from '../../components';
import { IUserItem, IUserItemResponse } from '../../types';
import { writeParamsInHistory } from '../../router';
import { getUsers } from '../../services';

export const Users = () => {
  // get initial state by history
  const locationParams = new URL(location.href).searchParams;
  let page = locationParams.get('page') ? Number(locationParams.get('page')) : 1;
  let value = locationParams.get('q') || '';

  const users = document.createElement('div');

  // handle form submit
  const submit = (q: string) => {
    if (value !== q) {
      value = q;
      page = 1;
      getData();
    }
  };

  const form = Search({ submit, placeholder: 'Search Users', value });
  users.appendChild(form);
  const usersContent = document.createElement('div');
  usersContent.className = 'user-content';
  const usersContentNode = users.appendChild(usersContent);

  // go to next page
  const nextClick = () => {
    page++;
    getData();
  };

  // go to prev page
  const prevClick = () => {
    page--;
    getData();
  };

  // mount users cards
  const mountRepos = (data: IUserItem[], count: number) => {
    usersContentNode.innerHTML = '';
    const prevDisabled = page === 1 || count < usersPerPages;
    const nextDisabled = count < usersPerPages || page * usersPerPages > count;
    const controls = Controls({ nextDisabled, nextClick, prevClick, prevDisabled });
    usersContentNode.prepend(controls);
    data.forEach((item) => {
      usersContentNode.appendChild(UserItem(item));
    });
  };

  const getData = () => {
    if (value) {
      // save state in history
      const newHistoryParams = `${routes.users}?page=${page}&q=${value}`;
      writeParamsInHistory(newHistoryParams);

      getUsers({ value, page })
        .then((res: IUserItemResponse) => {
          mountRepos(res.items, res.total_count);
        })
        .catch(() => {});
    }
  };
  getData();

  return users;
};
